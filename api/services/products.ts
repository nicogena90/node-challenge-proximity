import productModel  from '../schemes/product';
import * as mongoose from 'mongoose';
import { MongoMemoryServer } from "mongodb-memory-server";

export default class DbHelper {

	server: any;
	connection: any;
	constructor() {
			this.server = new MongoMemoryServer();
			this.connection = null;
	}

  /**
   * Start the server and establish a connection
   */
  async start() {

    const url = await this.server.getUri('server1_db1')
		
    await mongoose.connect(
      url,
      { useNewUrlParser: true, useUnifiedTopology: true }
    );
  }

  /**
   * Close the connection and stop the server
   */
  stop() {
    this.connection.close();
    return this.server.stop();
  }

	/**
	 * Insert products into the database and return it
	 * @param {string} collectionName
	 * @param {Object} documents
	 */
	async insertProducts(products: any) {

    try {

      return await productModel.insertMany(products);

    } catch (error) {
      throw new Error(error.message);
    }
	}

	/**
	 * Gets the productos from the data base
	 * @param {string} collectionName
	 */
	async getProducts() {

    try {

      return await productModel.find();

    } catch (error) {
      throw new Error(error.message);
    }
	}
}