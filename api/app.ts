import * as express from "express";
const app = express();

import appRoutes from "./routes";

app.use(express.json({limit: "50mb"}));

// Base route
const routes = [
  appRoutes
]

// Load the routes
app.use('/', routes);

export default app;
