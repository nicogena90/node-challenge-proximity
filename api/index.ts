import app from "./app"

const port = 5000;

app.listen(port, async () => {

  console.log(`Server initialized on port 5000`);
});
