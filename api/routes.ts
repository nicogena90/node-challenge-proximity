import * as productsController from "./controllers/products";

var express = require('express');
var api = express();
var multer  = require('multer');
var upload = multer({dest: 'tmp/csv/'});

api.post('/', upload.single('file'),        productsController.add);
api.get('/',                                productsController.get);

export default api;