declare namespace app.httpResponses {

  export const enum errorCode {
    OK = 200,
    BAD_REQUEST = 400,
    FORBIDDEN = 403,
    NOT_FOUND = 404,
    INTERNAL_SERVER_ERROR = 500
  }

  export const enum errorMsgs {
    MISSING_CSV_FILE = "CSV file is missing from the request",
    FILE_NOT_UPLOAD = "The file was not uploaded successfully",
    MISSING_CONFIG_FILE = "The configuration file does not exist",
  }

  export interface response {
    data?: any;
    errorMsg?: string;
  }
  
}