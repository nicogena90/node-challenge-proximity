import * as request from 'supertest'
import app from '../app'

const port = 5000
let server: any;

beforeAll( () => {
  server = app.listen(port);
})

afterAll(done => {
  server.close(done);
  done();
})

describe("test get products", () => {

  it('can get products', async (done) => {
    await request(server)
      .get('/')
      .expect(200);
      done();
  }),

  it('can post products', async () => {

    const filePath = `${__dirname}/../../example_csv/proximity.csv`;

    await request(server)
      .post('/')
      .attach('file', filePath)
      .expect(200)
  })

});
