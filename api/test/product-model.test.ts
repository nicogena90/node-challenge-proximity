import * as mongoose from 'mongoose'
import productModel from '../schemes/product'
import { MongoMemoryServer } from "mongodb-memory-server";

let server: any;

beforeAll(async () => {
  const server = new MongoMemoryServer();
  const url = await server.getUri('server1_db1')
  mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
  await productModel.deleteMany({})
})

afterEach(async () => {
  productModel.deleteMany({})
})

describe("product model test", () => {

  it('can create a product', async () => {
    await new productModel({ uuid: 'AA123' }).save()
    const productsCount = await productModel.countDocuments()
    expect(productsCount).toEqual(1)
  }),

  it('can fetch a product', async () => {
    const product = await new productModel({ uuid: 'AA1234' }).save()
    const fetchedProduct = await productModel.findOne({ _id: product.id })
    expect(product._id).toEqual(fetchedProduct._id)
  })

});
