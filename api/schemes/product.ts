import * as mongoose from 'mongoose';

/**
 * Product model schema
 */
const productSchema = new mongoose.Schema({
    uuid: { type: String},
    vin: { type: String},
    make: { type: String},
    model: { type: String},
    mileage: { type: Number},
    year: { type: Number},
    price: { type: Number},
    zip_code: { type: Number},
    create_date: { type: Date},
    update_date: { type: Date}
});

const productModel = mongoose.model('products', productSchema);
export default productModel;