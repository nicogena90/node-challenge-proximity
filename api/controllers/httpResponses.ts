import { Response } from "express";

/**
 * Generates and sends a response
 *
 * @param res           response
 * @param data          response data
 *
 */
export function success(res: Response, data: any): void {

  const response: app.httpResponses.response = {
    data
  };

  res.status(app.httpResponses.errorCode.OK).send({ response });
}

/**
 * Generates and sends an error response
 *
 * @param res           response
 * @param errorCode     error code
 * @param errorMsg      error message
 * 
 */
export function error(res: Response, errorCode: app.httpResponses.errorCode, errorMsg: string): void {

  const response: app.httpResponses.response = {
    errorMsg
  };

  console.error(errorMsg);
  res.status(errorCode).send({ response });
}
