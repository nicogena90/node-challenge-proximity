import { Request, Response } from "express";
import * as csv from 'fast-csv';
import * as fs from 'fs';
import * as httpResp from "./httpResponses";
import DbHelper from '../services/products'

const FILE_LOG = "controllers/products.ts";
const _CHUNK_SIZE_ = process.env._CHUNK_SIZE_ || 50;

const dbHelper = new DbHelper();

/**
 * Returns a product according to the config file
 * @param data              The data from the csv file
 * @param configFile        The configuration of the fields
 * @returns                 The product with only the fields in the configuration
 */
 function createProduct(data: any, configFile: any): any {

  const product: any = {};

  // We iterate the fields of the config file
  for (const field of configFile.fields) {
    if (data[field.name]) {

      product[field.name.toLowerCase().replace(' ', '_')] = data[field.name];
    }
  }

  return product;
 }

 /**
  * Validates the request and the csv and configuration files
  * @param req      Request
  * @returns        The result of the validation
  */
 function validate(req: any): any {

  // Verify if the csv file was recivied
  if (req.file == undefined) {

    return {
      errorType: app.httpResponses.errorCode.BAD_REQUEST,
      errorMessage: app.httpResponses.errorMsgs.MISSING_CSV_FILE,
      result: false
    }
  }

  // Create the path of the csv file uploaded
  const pathCSV = `${process.cwd()}/${req.file.path}`;

  // Verify if the file was stored correctly
  if (!fs.existsSync(pathCSV)) {

    return {
      errorType: app.httpResponses.errorCode.INTERNAL_SERVER_ERROR,
      errorMessage: app.httpResponses.errorMsgs.FILE_NOT_UPLOAD,
      result: false
    }
  }

  // Create the path of the configuration file
  const pathConfig = `${process.cwd()}/config/fields.json`;

  // We verify the file was stored correctly
  if (!fs.existsSync(pathConfig)) {

    return {
      errorType: app.httpResponses.errorCode.INTERNAL_SERVER_ERROR,
      errorMessage: app.httpResponses.errorMsgs.MISSING_CONFIG_FILE,
      result: false
    }

  }

  // Returns the result with no errors
  return { result: true }
 }

/**
 * Process the csv file and insert it to the data base
 *
 * @param req   Request
 * @param res   Response
 */
export async function add(req: any, res: Response) {
  
  try {

    // Validates the request and the existence of the files
    const validation = validate(req);

    // We check the validation
    if (!validation.result) {
      httpResp.error(res, validation.errorType, validation.errorMessage);
      return;
    }

    // Initialize constants 
    const pathCSV = `${process.cwd()}/${req.file.path}`;
    const pathConfig = `${process.cwd()}/config/fields.json`;
    const rawdata: Buffer = fs.readFileSync(pathConfig);
    const configFile = JSON.parse(rawdata.toString());

    // Initialize variables
    let rows: any = [];
    let count = 0;

    // Start database
    await dbHelper.start();

    // Read the csv of products
    fs.createReadStream(pathCSV)

      .pipe(csv.parse({ headers: true }))

      .on("error", (error: any) => { throw new Error(error.message); })
      
      .on("data", async (row: any) => {

        try {

          count++;
          const newProduct = createProduct(row, configFile);
          rows.push(newProduct);

          if (count >= _CHUNK_SIZE_) {

            dbHelper.insertProducts(rows);
            count = 0;
            rows = [];
          }

        } catch (error) {
          httpResp.error(res, app.httpResponses.errorCode.INTERNAL_SERVER_ERROR,
            `${FILE_LOG} (add): error (${error.stack})`);
        }
      })

      .on("end", async (totalCount: any) => {

        try {
  
          if (rows.length > 0) {
  
            await dbHelper.insertProducts(rows);
          }
  
          httpResp.success(res, `${totalCount} products were uploaded`);
          
        } catch (error) {
          httpResp.error(res, app.httpResponses.errorCode.INTERNAL_SERVER_ERROR,
            `${FILE_LOG} (add): error (${error.stack})`);
        }

    });   

  } catch (error) {
    httpResp.error(res, app.httpResponses.errorCode.INTERNAL_SERVER_ERROR,
      `${FILE_LOG} (add): error (${error.stack})`);
  }
}

/**
 * Gets all the items from a collection
 *
 * @param req   Request
 * @param res   Response
 */
 export async function get(req: any, res: Response) {
  
  try {

    await dbHelper.start();

    // Get the items from the collection of the parameter sent
    const products = await dbHelper.getProducts();
    
    httpResp.success(res, products);

  } catch (error) {
    httpResp.error(res, app.httpResponses.errorCode.INTERNAL_SERVER_ERROR,
      `${FILE_LOG} (add): error (${error.stack})`);
  }
}