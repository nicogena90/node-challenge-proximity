# node-challenge-proximity

Challenge of the company Proximity to assess knowledge about Node

## Initialize the application with the script
```
sh dev-init.sh 
```

OR

Step by step
1) Go to the api folder of the app
```
cd node-challenge-proximity/api
```

2) Install dependencies
```
npm i
```

3) Start api
```
npm run start-dev
```

Example of csv file in `node-challenge-proximity/example csv/proximity.csv`
If the csv file has a column that is not in the config file it won´t be proceed to be stored in the data base

Configuration file in `node-challenge-proximity/api/config/fields.json`

with the following format:
```
{
	"fields": [
    {
      "name": "UUID"
    },
    {
      "name": "VIN"
    },
    {
      "name": "Make"
    },
    {
      "name": "Model"
    },
    {
      "name": "Mileage"
    },
    {
      "name": "Year"
    },
    {
      "name": "Price"
    },
    {
      "name": "Zip Code"
    },
    {
      "name": "Create Date"
    },
    {
      "name": "Update Date"
    }
  ]   
}
```

## These are the endpoint:
```
POST  localhost:5000/  -> Saves a product in teh data base and the property of the body with the files needs to be named `file`
GET   localhost:5000/  -> Gets all the products from the data base
```

## Testing
To run the tests
```
npm test
```

This project was programmed in typescript and were used the followings libraries:
- express                   https://www.npmjs.com/package/express
- @types/node               https://www.npmjs.com/package/@types/node
- @types/express            https://www.npmjs.com/package/@types/express
- multer                    https://www.npmjs.com/package/multer
- mongodb-memory-server     https://www.npmjs.com/package/mongodb-memory-server
- fs                        https://www.npmjs.com/package/fs
- fast-csv                  https://www.npmjs.com/package/fast-csv
- mongoose                  https://www.npmjs.com/package/mongoose